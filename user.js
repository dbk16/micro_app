import express from  'express';
import bodyParser from  'body-parser';
import {userslist} from './app/controllers';
import {mongoose,url} from './app/models';
const app = express();
const router = express.Router()

const urlencodedParser = bodyParser.urlencoded({ extended: true });
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

router.get('/',urlencodedParser,userslist);

app.use('/users',router)

mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
// set port, listen for requests
app.listen(3000, () => {
  console.log(`Server is running on port 3000`);
});