const dbConfig = require("../config/db.config.js");
const mongoose = require("mongoose");
const users = require("./user.model.js")(mongoose);
mongoose.Promise = global.Promise;
const {url}=dbConfig
const db = {
	mongoose,
	url,
	users
}
module.exports = db