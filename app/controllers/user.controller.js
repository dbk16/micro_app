import {users} from '../models';
async function userslist(req,res){	
	users.find()
    .then(data => {
      res.send({ status:1,data});
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
}
async function accountActivation(req,res){ 
  const id = req.params.id; 
  users.findByIdAndUpdate(id,{'activeStatus':true}, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          status:0,
          message: `Cannot activate User. Maybe User was not found!`
        });
      } else res.send({ status:1, message: "Account activated successfully." });
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
}
module.exports={
  userslist: userslist,
  accountActivation: accountActivation
}