const signupController=require('./signup.controller');
const loginController=require('./login.controller');
const userController=require('./user.controller');
module.exports={
	userSignup: signupController.signup,
	userLogin: loginController.login,
	userslist: userController.userslist,
	accountActivation: userController.accountActivation,
}
