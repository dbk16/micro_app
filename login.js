import express from  'express';
import bodyParser from  'body-parser';
import {userSignup,userLogin,accountActivation} from './app/controllers';
import {mongoose,url} from './app/models';
const app = express();
const router = express.Router()

const urlencodedParser = bodyParser.urlencoded({ extended: true });
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
router.get("/", (req, res) => {
  res.json({ message: "Welcome to micro application." });
});
router.post('/register',urlencodedParser,userSignup);
router.post('/login',urlencodedParser,userLogin);
router.get('/user/:id',urlencodedParser,accountActivation);

app.use('/auth',router)
mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
// set port, listen for requests
app.listen(5000, () => {
  console.log(`Server is running on port 5000`);
});